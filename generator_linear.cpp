#include <bits/stdc++.h>
using namespace std;

const int MAX = 1e6;

int N, M;

int get() {
    return rand() % N;
}

int main() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);

    cin >> N;
    cout << N << " " << N - 1 << "\n";

    for (int i = 0; i < N - 1; i++) {
        cout << i << " " << i + 1 << "\n";
    }
}
