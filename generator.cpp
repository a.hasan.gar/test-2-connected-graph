#include <bits/stdc++.h>
using namespace std;

const int MAX = 1e6;

int N, M;
set<int> adj[MAX+5];

int get() {
    return rand() % N;
}

int main() {
	ios_base::sync_with_stdio(0);
	cin.tie(0);
	cout.tie(0);
	
    srand(time(0));
    cin >> N >> M;

    cout << N << " " << M << "\n";

    for (int i = 0; i < M; i++) {
        int u = get(), v = get();
        int mn = min(u, v), mx = max(u, v);

        while (mn == mx || adj[mn].count(mx) > 0) {
        	u = get(), v = get();
        	mn = min(u, v), mx = max(u, v);
        }

        cout << u << " " << v << "\n";
        adj[mn].insert(mx);
    }
}
